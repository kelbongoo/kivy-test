from kivy.uix.listview import ListItemButton

class LogisticsItemButton(ListItemButton):
    pass

class LogisticsItemPickupButton(LogisticsItemButton):
    pass
