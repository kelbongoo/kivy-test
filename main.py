from kivy.app import App

import sqlite3
import datetime

from kivy.uix.label import Label
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.widget import Widget

from kivy.uix.button import Button

from kivy.uix.listview import ListItemButton

from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.properties import NumericProperty, ReferenceListProperty,\
    ObjectProperty, StringProperty, BooleanProperty, DictProperty, ListProperty
# from numberpad import NumPad

POINTDEPOT = 'BIN'
conn = sqlite3.connect('locale.db')
c = conn.cursor()

# c.execute("DROP TABLE producers")
# c.execute("DROP TABLE globalorderproducerproducts")
# c.execute("DROP TABLE logisticsitemtransferitems")
# c.execute("DROP TABLE temperatures")
# c.execute("DROP TABLE logisticsitems")

# c.execute('''CREATE TABLE producers
#               (_id text, name text, delivery_code text, delivered_at date)''')
# c.execute('''CREATE TABLE globalorderproducerproducts
#               (producerproduct_id text, producer_id text, product_name text, unit_display text, take_temperature integer)''')
# c.execute('''CREATE TABLE logisticsitemtransferitems
#              (logisticsitem text, type text, quantity_ordered integer, quantity_expected integer, quantity_received integer, producer_id text)''')
# c.execute('''CREATE TABLE temperatures
#              (code text, value real, date date, meta text)''')
# c.execute('''CREATE TABLE logisticsitems
#              (code text, name text)''')

# c.execute("INSERT INTO producers VALUES ('producer_id','producer','1234', ?)", (datetime.datetime.now(),))
c.execute("INSERT INTO producers VALUES ('producer_id','producer with a very long name','1234', null)")
c.execute("INSERT INTO globalorderproducerproducts VALUES ('producerproduct_id','producer_id','product_name','1 kg', 1)")
c.execute("INSERT INTO logisticsitemtransferitems VALUES ('BGC','pickup', 10, null, null, 'producer_id')")
c.execute("INSERT INTO logisticsitems VALUES ('BGC','grande caisse verte')")
c.execute("INSERT INTO logisticsitems VALUES ('SGC','petite caisse verte')")


# CodeScreen handles the input/validation of producer codes
# and sets the global current producer property

class CodeScreen(Screen):
	producer_code = StringProperty('')

	def confirm_code(self, app, code):
		if code == '':
			return
		self.producer_code = code
		self.validate_producer_code(app, code)


	def validate_producer_code(self, app, code):
		# fetch producers from sqlite, check the hashed code
		# if incorrect, show message, if correct, set app current_producer

		c.execute("SELECT * FROM producers WHERE delivery_code=?", (code,))
		producer = c.fetchone()

		if producer is None:
			self.ids.not_found_label.opacity = 1

		self.ids.not_found_label.opacity = 0
		self.ids.numpad_code.reset()
		self.producer_code = ''

		c.execute("SELECT * FROM logisticsitems")
		logisticsitems = c.fetchall()

		c.execute("SELECT * FROM logisticsitemtransferitems WHERE producer_id = ?", (producer[0],))
		logisticsitemtransferitems = c.fetchall()

		c.execute("SELECT * FROM globalorderproducerproducts WHERE producer_id = ?", (producer[0],))
		gopps = c.fetchall()

		app.set_current_data(producer, logisticsitems, logisticsitemtransferitems, gopps)

		# if None is None:
		# 	self.ids.not_found_label.opacity = 1
		# else:
		# 	self.ids.not_found_label.opacity = 0
		# 	self.producer = producer
        #
		# 	cursor.execute('''SELECT * FROM realcircuitdeliveries WHERE producer_id=? AND realcircuit_id = ?''', (producer._id, app.realcircuit._id))
		# 	gopo = cursor.fetchone()
        #
		# 	if gopo is None:
		# 		app.root.current = 'delivery-not-found'
		# 	else:
		# 		self.producer_order = gopo
        #
		# 		if hasattr(a, 'picked_up'):
		# 			app.root.current = 'already-delivered'
		# 		else:
		# 			app.producer = producer
		# 			app.producer_delivery = gopo
		# 			app.root.current = 'material-leave'
#
#
# class MaterialLeaveScreen(Screen):
# 	# should have default 'expected leave' ? or should let producer fill in ?
# 	producer = ObjectProperty(app.producer)
# 	material_leave = ObjectProperty(app.producer_delivery.material_leave)
#
# 	def change_material_leave(self, property, value):
# 		self.material_leave[property] = value
#
# 	def validate_material_leave(self, *args):
# 		# fetch producers from sqlite, check the hashed code
# 		# if incorrect, show message, if correct, set app current_producer
# 		app.root.current = 'material-pickup'
#
# class MaterialPickupScreen(Screen):
# 	# should have default 'expected leave'
# 	material_leave = ObjectProperty()
#
# 	def change_material_pickup(self, value):
# 		self.material_leave = value
#
# 	def validate_material_pickup(self, *args):
# 		# fetch producers from sqlite, check the hashed code
# 		# if incorrect, show message, if correct, set app current_producer
# 		if(current_delivery.temperature_check):
# 			app.root.current = 'temperature'
# 		else:
# 			app.root.current = 'summary'
#
# class TemperatureScreen(Screen):
# 	# should have default 'expected leave' ? or should let producer fill in ?
# 	temperature = NumberProperty()
#
# 	def set_temperature(self, value):
# 		self.temperature = value
#
# 	def validate_temperature(self, *args):
# 		# fetch producers from sqlite, check the hashed code
# 		# if incorrect, show message, if correct, set app current_producer
# 		app.root.current = 'summary'
#
# class SummaryScreen(Screen):
# 	# the results of this should be sent to the producer by email ?
#
# 	def validate(self, *args):
# 		# fetch producers from sqlite, check the hashed code
# 		# if incorrect, show message, if correct, set app current_producer
# 		app.root.current = 'thankyou'
#

# class Producer(EventDispatcher):
#     name = StringProperty('')
#     _id = StringProperty('')
#     delivery_code = StringProperty('')
#     take_temperature = IntegerProperty(0)
#
# 	def __init__(self, producer):
# 		self.name = producer[0]
# 		self._id = producer[1]
# 		self.delivery_code = producer[2]
# 		self.take_temperature = producer[3]

class DoneScreen(Screen):
	# this screen should last for X seconds
	pass

class AlreadyDeliveredScreen(Screen):
	pass

class ErrorScreen(Screen):
	pass

class BLScreen(Screen):
	pass

class LeaveHomeScreen(Screen):
	pass
	# items = ListProperty([])

	# def update_list(self, app):
	# 	self.items = app.

class PickupHomeScreen(Screen):
	def args_converter(self, row_index, obj):
		return {'text': obj['name'] }

	def get_current_logisticsitems(self, logisticsitems):
		output = []
		for item in logisticsitems:
			print(item.has_key('type'))
			if item.has_key('type') and item['type'] == 'pickup':
				output.append(item)
		return output


class LogisticsItemScreen(Screen):

	def confirm_count(self, app, count):
		app.set_current_logisticsitem_count(count)

class TemperatureScreen(Screen):
	product_name = StringProperty('')
	current_temperature = NumericProperty(None)

	def reset_current_temperature(self, app):
		self.current_temperature = None
		app.reset_current_temperature()

	# called via bluetooth module
	def set_current_temperature(self, app, temperature):
		self.current_temperature = temperature



class HomeScreen(Screen):
	pass

class IconButton(Button):
	icon = StringProperty("warning.png")

class NumPad(Widget):
    display_text = StringProperty("")
    confirmed_text = StringProperty("")

class NumPadCode(NumPad):

    def reset(self):
        self.confirmed_text = ''
        self.display_text = ''

    def button_callback(self, button_str):
        if button_str in [str(x) for x in range(10)]:
            if len(self.display_text) < 4:
                self.display_text = self.display_text + button_str
        elif button_str == 'del':
            self.display_text = self.display_text[:-1]
        elif button_str == 'ok':
            self.confirmed_text = self.display_text

class NumPadCount(NumPad):
    def button_callback(self, button_str):
        if button_str in [str(x) for x in range(10)]:
            temp = self.display_text + button_str
            if int(temp) > 0 and int(temp) < 200:
                self.display_text = self.display_text + button_str
        elif button_str == 'del':
            self.display_text = self.display_text[:-1]
        elif button_str == 'ok':
            self.confirmed_text = self.display_text

class HomeButton(Button):
    validated_bg = (0,1,0,0.7)
    unvalidated_bg = (1,0.6,0,0.7)
    hidden = BooleanProperty(True)
    is_validated = BooleanProperty(True)

class TerminalApp(App):

	current_producer = DictProperty({ 'name': '', '_id': '' })
	current_temperature_gopp = DictProperty({ 'product_name': '' })
	current_logisticsitems = ListProperty([])
	current_logisticsitem = DictProperty({ 'name': '', 'code': '', 'type': '', 'quantity_expected': ''})
	current_gopps = ListProperty([])
	bl_validated = BooleanProperty(False)
	leave_validated = BooleanProperty(False)
	pickup_validated = BooleanProperty(False)
	current_temperature = NumericProperty(None)
	all_validated = BooleanProperty(False)

	def load_current_producer(self, producer_data):
		self.current_producer['_id'] = producer_data[0]
		self.current_producer['name'] = producer_data[1]
		return { '_id': producer_data[0],
			'name': producer_data[1],
			'delivery_code': producer_data[2],
			'delivered_at': producer_data[3] }

	def load_current_logisticsitems(self, logisticsitems, logisticsitemtransferitems):
		output = []
		for item in logisticsitems:
			dict = { 'code': item[0], 'name': item[1] }
			for titem in logisticsitemtransferitems:
				if titem[0] == item[0]:
					dict['type'] = titem[1]
					dict['quanity_ordered'] = titem[2]
					dict['quanity_expected'] = titem[3]
					dict['quanity_received'] = titem[4]
			output.append(dict)
		self.current_logisticsitems = output

	def load_current_gopps(self, gopps):
		output = []
		for gopp in gopps:
			dict = { 'producerproduct_id': gopp[0],
				'producer_id': gopp[1],
				'product_name': gopp[2],
				'unit_display': gopp[3],
				'take_temperature': True if gopp[4] == 1 else False }
			output.append(dict)
		self.current_gopps = output

	def load_current_temperature_gopp(self, gopps):
		for gopp in gopps:
			if gopp[4] == 1:
				self.current_temperature_gopp = { 'producerproduct_id': gopp[0],
					'producer_id': gopp[1],
					'product_name': gopp[2],
					'unit_display': gopp[3] }

	def validate_current_temperature(self, temperature):
		self.current_temperature = temperature

	def reset_current_temperature(self):
		self.current_temperature = None

	def allow_producer_updates(self):
		self.sm.current = 'home'
		self.remove_producer_delivered_at()


	def validate_bl(self, result):
		self.bl_validated = result
		self.sm.transition.direction = 'right'
		self.sm.current = 'home'

	def get_current_logisticsitem(self):
		return self.current_logisticsitem

	def show_logisticsitem_detail(self, type, name):
		for item in self.current_logisticsitems:
			if item['name'] == name:
				self.current_logisticsitem = item
		self.sm.transition.direction = 'left'
		self.sm.current = 'logisticsitem'

	def set_current_data(self, producer_data, logisticsitems, logisticsitemtransferitems, gopps):

		producer = self.load_current_producer(producer_data)
		self.load_current_logisticsitems(logisticsitems, logisticsitemtransferitems)
		self.load_current_gopps(gopps)
		self.load_current_temperature_gopp(gopps)

		if producer['delivered_at'] is not None:
			self.sm.current = 'alreadydelivered'
		else:
			self.sm.current = 'home'

	def get_temperature_product_name(self, gopps):
		output = ''
		for gopp in gopps:
			if gopp[4] == True:
				output = gopp[2]
		return output

	def set_current_logisticsitem_count(self, count):
		current_type = self.current_logisticsitem['type']
		current_code = self.current_logisticsitem['code']
		for item in self.current_logisticsitems:
			if item['code'] == current_code and item['type'] == current_type:
				item['quantity_final'] = count
		c.execute("UPDATE logisticsitemtransferitems SET quantity_received = ? WHERE logisticsitem = ? AND type = ?",(count, current_code, current_type))
		self.sm.current = self.current_logisticsitem['type']+"home"
		self.sm.transition.direction = 'right'
		self.current_logisticsitem = {}

	def reset(self):
		self.current_producer = { 'name': '', '_id': '' }
		self.take_temperature = 0
		self.current_logisticsitemtransferitems = []
		self.current_gopps = []
		self.bl_validated = False
		self.leave_validated = False
		self.pickup_validated = False
		self.temperature_validated = False

	def remove_producer_delivered_at(self):
		self.current_producer['delivered_at'] = None
		c.execute("UPDATE producers SET delivered_at = ? WHERE _id = ?",(None, self.current_producer['_id']))

	def transition(self, direction, screen):
		self.sm.transition.direction = direction
		self.sm.current = screen

	def build(self):

		root = BoxLayout(orientation='vertical')
		self.sm = sm = ScreenManager()
		codeScreen = CodeScreen()
		homeScreen = HomeScreen()
		blScreen = BLScreen()
		leaveHomeScreen = LeaveHomeScreen()
		logisticsItemScreen = LogisticsItemScreen()
		pickupHomeScreen = PickupHomeScreen()
		temperatureScreen = TemperatureScreen()
		errorScreen = ErrorScreen()
		alreadyDeliveredScreen = AlreadyDeliveredScreen()
		doneScreen = DoneScreen()
		sm.add_widget(codeScreen)
		sm.add_widget(homeScreen)
		sm.add_widget(blScreen)
		sm.add_widget(leaveHomeScreen)
		sm.add_widget(pickupHomeScreen)
		sm.add_widget(logisticsItemScreen)
		sm.add_widget(temperatureScreen)
		sm.add_widget(errorScreen)
		sm.add_widget(alreadyDeliveredScreen)
		sm.add_widget(doneScreen)
		root.add_widget(sm)
		return root

	# def update_label(self,_,text):
	# 	self.l.color=text


if __name__ == "__main__":
	TerminalApp().run()
