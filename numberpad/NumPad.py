import kivy

from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import StringProperty

class NumPad(Widget):
    display_text = StringProperty("0")

    def button_callback(self, button_str):
        if button_str in [str(x) for x in range(10)]:
            if self.display_text.length <= 4:
                self.display_text = self.display_text + button_str
        elif button_str == 'del':
            self.display_text = self.display_text[:-1]
        elif button_str == 'ok':
            self.set_code(code=self.display_text)